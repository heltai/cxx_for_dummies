#include <iostream>

#include "triangle_cell.h"
#include <vector>

using namespace CxxForDummies;

void print_n_vertices(const BaseCell &cell) {
  std::cout << "Cell has " << cell.n_vertices() << " vertices " << std::endl;
  std::cout << "Address is: " << (long int) &cell << std::endl;
  const TriangleCell * d = dynamic_cast<const TriangleCell *>(&cell);
  if( d != NULL) {
    const TriangleCell &e = *d;
    std::cout << "Cell is a Triangle" << std::endl;
    e.print_ciao();
  }
  std::cout << "------------------------------------------------------------" << std::endl;
}


int main() {
    std::cout << "Hello guys!" << std::endl;

    TriangleCell cell;
    print_n_vertices(cell);

    const BaseCell &b2 = cell;
    print_n_vertices(b2);

    const BaseCell &b = static_cast<const BaseCell>(cell);
    print_n_vertices(b);

    const BaseCell *c = dynamic_cast<const BaseCell *>(&cell);
    print_n_vertices(*c);

    const TriangleCell *d = dynamic_cast<const TriangleCell *> (&b);
    if( d == NULL) {
      std::cout << "Cannot convert " << (long int) &b << " to TriangleCell" << std::endl;
    }
    else {
      print_n_vertices(*d);
    }
}
